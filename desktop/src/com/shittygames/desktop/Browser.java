package com.shittygames.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.shittygames.utils.TextureBrowser;

public class Browser {
    public static void main(String []args){
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width=1280;
        config.height=720;
        new LwjglApplication(new TextureBrowser(), config);
    }
}
