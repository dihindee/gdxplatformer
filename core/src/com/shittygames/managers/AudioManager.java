package com.shittygames.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class AudioManager{
    private static Music[] musicList;
    private static Sound[] soundList;

    public static void Initialize() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new FileInputStream("music.list"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        musicList = new Music[scanner.nextInt()];
        for (int i = 0; i < musicList.length; i++) {
            musicList[i] = Gdx.audio.newMusic(Gdx.files.internal(scanner.next()));
        }
        scanner.close();
        try {
            scanner = new Scanner(new FileInputStream("sounds.list"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        soundList = new Sound[scanner.nextInt()];
        for (int i = 0; i < soundList.length; i++) {
            soundList[i] = Gdx.audio.newSound(Gdx.files.internal(scanner.next()));
        }
        scanner.close();
    }

    public static void playMusic(int id) {
        if (!musicList[id].isPlaying()) musicList[id].play();
    }

    public static void setMusicVolume(int id, float volume) {
        musicList[id].setVolume(volume);
    }

    public static void pauseMusic(int id) {
        musicList[id].pause();
    }

    public static void stopMusic(int id) {
        musicList[id].stop();
    }

    public static void setMusicLooping(int id, boolean isLooping) {
        musicList[id].setLooping(isLooping);
    }

    public static void stopAllMusic() {
        for (int i = 0; i < musicList.length; i++) musicList[i].stop();
    }

    public static void pauseAllMusic() {
        for (int i = 0; i < musicList.length; i++) musicList[i].pause();
    }

    /*
     pitch is a sound speed (0.5f - 2.0f)
     pan is position of sound (-1 for only left ear and 1 for only right ear)
     */
    public static void playSound(int id, float volume, float pitch, float pan) {
        soundList[id].play(volume, pitch, pan);
    }

    public static void loopSound(int id, float volume, float pitch, float pan) {
        soundList[id].loop(volume, pitch, pan);
    }

    public static void pauseSound(int id) {
        soundList[id].pause();
    }

    public static void stopSound(int id) {
        soundList[id].stop();
    }

    public static void stopAllSounds() {
        for (int i = 0; i < soundList.length; i++) soundList[i].stop();
    }

    public static void pauseAllSounds() {
        for (int i = 0; i < soundList.length; i++) soundList[i].pause();
    }

    public static void dispose() {
        for (int i = 0; i < musicList.length; i++) musicList[i].dispose();
        for (int i = 0; i < soundList.length; i++) soundList[i].dispose();
    }
}
