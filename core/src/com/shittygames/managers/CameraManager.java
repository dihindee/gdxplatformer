package com.shittygames.managers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.shittygames.actors.MyActor;

public class CameraManager {
    static final int CAMERA_WIDTH=1280,CAMERA_HEIGHT=720;
    enum CameraMode{STATIC,TARGET}
    private static MyActor target;
    private static CameraMode cameraMode = CameraMode.STATIC;
    private static OrthographicCamera camera;
    public static float zoom=1,fluency=1;
    public static void InitCamera(){
        camera = new OrthographicCamera();
        camera.setToOrtho(false);
        camera.viewportHeight = CAMERA_HEIGHT;
        camera.viewportWidth = CAMERA_WIDTH;
    }
    public static void CamUpdate(){
    if(cameraMode==CameraMode.TARGET){
        camera.translate((target.getX()+target.getWidth()-camera.position.x)/fluency,
                (target.getY()+target.getHeight()/2-camera.position.y));

        }
        camera.update();
    }
    public static void CamFollow(MyActor actor){
    target = actor;
    cameraMode = CameraMode.TARGET;
    }
    public static void LookAt(float x, float y){
        target = null;
        cameraMode = CameraMode.STATIC;
        camera.translate(x-camera.position.x,y-camera.position.y);

    }
    public static void SetCamScale(float scale){
         zoom*= scale;
    camera.viewportHeight = CAMERA_HEIGHT*zoom;
    camera.viewportWidth = CAMERA_WIDTH*zoom;
    }
    public static void SetAbsCamScale(float absScale){
        zoom = absScale;
        camera.viewportHeight = CAMERA_HEIGHT*zoom;
        camera.viewportWidth = CAMERA_WIDTH*zoom;
    }
    public static void SetFluency(int CamFluency){
        if(CamFluency>0)
        fluency = CamFluency;
        else {
            System.out.println("wrong fluency value");
            fluency=1;
        }
    }
    public static float GetCamX(){
        return camera.position.x;
    }
    public static float GetCamY(){
        return camera.position.y;
    }
    public static float GetViewportWidth(){
        return camera.viewportWidth;
    }
    public static float GetViewportHeight(){
        return camera.viewportHeight;
    }

    public static OrthographicCamera GetCamera(){
        return camera;
    }
}
