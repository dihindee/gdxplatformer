package com.shittygames.actors;

import com.shittygames.managers.ActorManager;
import com.shittygames.managers.CollisionManager;
import com.shittygames.utils.AnimatedTexture;
import com.shittygames.utils.EventClass;
import com.shittygames.utils.EventOnHit;
import com.shittygames.utils.Trajectory;

import static com.shittygames.managers.ActorManager.Add;

public class Fireball extends MyActor implements EventOnHit {
    static EventClass onHit = origin -> {
        System.out.println("boom");
        Add(new StaticObject(origin.x - 32, origin.y - 32, 64, 64, "explosion-4", 12, .1f, AnimatedTexture.PlayMode.NORMAL, 1, null, 12));
        Add(new DamageBox(origin.x - 32, origin.y - 32, 64, 64, ((Fireball) origin).damage, DamageBox.DMG_ALL));
        //AudioManager.playSound(0,1,0,0);
        origin.Destroy();
    };
    private FTrajectory trajectory;
    public float damage;
    public EventClass explode;
    int damagetype;

    public Fireball(float x, float y, float size,float damage,boolean toRight, String texture, int damageType, int layer) {
        super(x, y, size, size, texture, CollisionManager.FIREBALL, null, layer);
        this.damagetype = damageType;
        this.trajectory = new FTrajectory(x, y, toRight);
        this.explode = onHit;
        this.damage = damage;
        //System.out.println(this.getScript());
    }

    @Override
    public void update(float delta){
        texture.update(delta);
        trajectory.update(delta);
        this.x = trajectory.x;
        this.y = trajectory.y;
        //this.eventOnHit();
        ActorManager.CheckTouches(this,delta);

    }

    @Override
    public void eventOnHit() {
        onHit.event(this);
    }


    private class FTrajectory extends Trajectory
    {
        public FTrajectory(float x0, float y0, boolean positive) {
            super(x0, y0, positive);
        }

        @Override
        public void updateX() {
            x = x0 + 800 * t;
        }

        @Override
        public void updateY() {
            y = y0;
        }
    }
}
