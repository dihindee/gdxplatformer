package com.shittygames.actors;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.shittygames.managers.ActorManager;
import com.shittygames.managers.TextureManager;
import com.shittygames.utils.AnimatedTexture;

import java.util.Objects;

/**
 * Created by admin on 18.08.2017.
 */
public class MyActor {

    protected float x;
    protected float y;
    protected float width;
    protected float height;
    public float vx = 0, vy = 0;
    private boolean isMoving = false, kill = false;
    boolean flipX = false, flipY = false;
    private float tx, ty;
    private int script;
    AnimatedTexture texture;
    public String name;
    public int layer;

    //Texture texture;
    public MyActor(float x, float y, float width, float height, String texture, int script, String name, int layer) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.script = script;
        if (texture != null)
            this.texture = new AnimatedTexture(texture,1,1, AnimatedTexture.PlayMode.LOOP);
        else this.texture = null;
        this.name = name;
        this.layer = layer;
        //texture = MyGdxGame.texture;
    }

    public MyActor(float x, float y, float width, float height, String texture, int frames, float frameDuration,AnimatedTexture.PlayMode playMode, int script, String name, int layer) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.script = script;
        this.texture = new AnimatedTexture(texture,frames,frameDuration,playMode);
        this.name = name;
        this.layer = layer;
    }

    public void setScript(int script) {
        this.script = script;
    }

    public void update(float delta) {
        texture.update(delta);
        if(texture.isEnded())this.Destroy();
        // System.out.println(this.vx + "  " + this.vy);
        if (isMoving) {
            // System.out.println(this.name + ":  "+this.vx + "  " + this.vy);
            if (Math.abs(tx - x) <= Math.abs(vx * delta) && Math.abs(ty - y) <= Math.abs(vy * delta)) {
                vx = vy = 0;
                isMoving = false;
                x = tx;
                y = ty;
            } else {
                x += vx * delta;
                y += vy * delta;
            }
        }
    }

    public void draw(SpriteBatch batch) {
        if (texture != null) {
            batch.draw(texture.getFrame(), flipX ? x - width : x, flipY ? y - height : y, width, height);
        }

    }

    public void moveTo(float x, float y, float time) {
        if (time == 0) {
            this.x = x;
            this.y = y;
            return;
        }
        if ((this.x != x) || (this.y != y)) {
            //System.out.println("x = [" + x + "], y = [" + y + "], time = [" + time + "]");
            tx = x;
            ty = y;
            vx = (x - this.x) / time;
            vy = (y - this.y) / time;
            isMoving = true;
        }
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    /*int getTexture() {
        return texture;
    }*/

    public int getScript() {
        return script;
    }

    //public void setFriction(float friction){this.friction = friction;}
    //public float getFriction(){return friction;}
    public void Destroy() {
        kill = true;
    }

    public boolean isDestroying() {
        return kill;
    }

    @Override
    public boolean equals(Object object) {
        return Objects.equals(this.name, object.toString());
    }

    @Override
    public String toString() {
        return this.name;
    }

    public boolean isMoving() {
        return isMoving;
    }
}
