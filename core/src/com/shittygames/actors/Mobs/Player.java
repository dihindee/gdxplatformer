package com.shittygames.actors.Mobs;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.shittygames.actors.DamageBox;
import com.shittygames.actors.Fireball;
import com.shittygames.managers.ActorManager;
import com.shittygames.managers.CollisionManager;
import com.shittygames.managers.TextureManager;
import com.shittygames.actors.StatusBar;

import static com.shittygames.managers.ActorManager.Add;
import static com.shittygames.managers.ActorManager.CheckTouches;
import static com.shittygames.screens.Levels.TestStage.G;
import static com.shittygames.actors.Mobs.Mob.airPos.FALLING;
import static com.shittygames.actors.Mobs.Mob.airPos.GROUNDED;
import static com.shittygames.actors.Mobs.Mob.airPos.JUMPING;

public class Player extends Mob{
    //private static final int data[][] = {{32,48,900,1,96,144},{50,60,150,1,120,135},{60,80,100,5,135,165}};

    private static final int data[][] = {{32,48,900,1,96,144},{35,62,100,4,70,120},{60,80,100,5,135,165}};
    //textureWidth,textureHeight,delay,playMode,drawWidth,drawHeight
    //private static final String name[] = {"player-idle","player-run","player-jump"};
    private static final String name[] = {"player-idle","skeleton","player-jump"};
    //region names
    private static final float V_HOR = 500,V_JUMP =1000;
    private static final int def_w=92,def_h=128,script = CollisionManager.PLAYER;
    /*
    playMode:
    1 LOOP
    2 LOOP_PINGPONG
    3 LOOP_RANDOM
    4 LOOP_REVERSED
    5 NORMAL
    6 REVERSED
     */

    private static Animation mov[];
    private int mov_ptr =0;

    public Player(float x, float y, String name, int layer) {
        super(x, y, def_w,def_h, null, script, name, layer);
        setParams(100,100,100,150,1,1);
        //ActorManager.Add(new StatusBar(this,false));
    }
    public Player(float x,float y,int layer){
        super(x,y,def_w,def_h,null,script,"player",layer);
        setParams(100,100,100,150,1,1);
        //ActorManager.Add(new StatusBar(this,false));
    }

    @Override
    public void update(float delta) {
        if(MP<maxMP)MP+=1;
        if(HP<maxHP)HP+=1;
        vl = vr = V_HOR*horMul;
        vy -= G;
        gndvx=0;
        pos = airPos.FALLING;
        ActorManager.CheckTouches(this,delta);
        if (Gdx.input.isKeyPressed(Input.Keys.A)) moveLeft();
        if (Gdx.input.isKeyPressed(Input.Keys.D)) moveRight();
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) jump();
        if (Gdx.input.isKeyPressed(Input.Keys.F)&&this.MP>=25){
            Add(new Fireball(this.x+width+20,this.y+height/2,32,10,this.Right,"fireball", DamageBox.DMG_ALL,12));
            this.MP-=25;
        }

        if(pos==GROUNDED&&!Gdx.input.isKeyPressed(Input.Keys.ANY_KEY)&&mov_ptr!=0){mov_ptr=0;timer=0;}
        if(HP<=0)this.Destroy();
        x += vx*delta;
        y += vy*delta;
        // if(vx!=0)
        timer +=delta;
        if(HP<0){this.Destroy();Gdx.app.exit();}
    }

    @Override
    public void draw(SpriteBatch batch) {
       // batch.draw(mov[mov_ptr].getKeyFrame(timer),getX(),getY(),data[mov_ptr][4],data[mov_ptr][5]);
        batch.draw(mov[mov_ptr].getKeyFrame(timer),Right?x:x+data[mov_ptr][4]
                ,y,data[mov_ptr][4]*(!Right?-1:1),data[mov_ptr][5]);
    }

    @Override
    public void jump() {
        if(pos==GROUNDED){
            y+=1;
            vy+=V_JUMP*jMul;pos = JUMPING;
            mov_ptr=2;
            timer=0;}
    }

    @Override
    void moveLeft() {
        vx=gndvx - vl;
        Right = false;
        if(vl>0&&mov_ptr!=1&&pos!=FALLING){mov_ptr=1;timer=0;}
    }

    @Override
    void moveRight() {
        vx=gndvx + vr;
        Right = true;
        if(vr>0&&mov_ptr!=1&&pos!=FALLING){mov_ptr=1;timer=0;}
    }

    public static void LoadAssets(){
        mov = new Animation[name.length];
        //TextureRegion[] region = TextureManager.getTextures(files);
        TextureRegion[] frames;
        //mov = new Animation[files.length-1];
        for (int i=0;i<mov.length;i++){
            frames = TextureManager.getRegion(TextureManager.getRegionIndex(name[i])).split(data[i][0],data[i][1])[0];

            mov[i] = new Animation(data[i][2]/1000f,frames);
            switch (data[i][3]){
                case 1:{mov[i].setPlayMode(Animation.PlayMode.LOOP);break;}
                case 2:{mov[i].setPlayMode(Animation.PlayMode.LOOP_PINGPONG);break;}
                case 3:{mov[i].setPlayMode(Animation.PlayMode.LOOP_RANDOM);break;}
                case 4:{mov[i].setPlayMode(Animation.PlayMode.LOOP_REVERSED);break;}
                case 5:{mov[i].setPlayMode(Animation.PlayMode.NORMAL);break;}
                case 6:{mov[i].setPlayMode(Animation.PlayMode.REVERSED);break;}
            }
        }
    }
}
