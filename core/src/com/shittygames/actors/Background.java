package com.shittygames.actors;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.shittygames.managers.CameraManager;
import com.shittygames.managers.TextureManager;
import com.shittygames.utils.AnimatedTexture;

public class Background extends MyActor {
    public static final int CAM_SIZE= -1;
    enum DrawMode {PAVE, STRETCH}
    private DrawMode mode = DrawMode.STRETCH;
    private float tileWidth,tileHeight,deltaX=0,deltaY=0;
    private boolean customH=false,customW=false;
    private Parallax parallax=null;
    public Background(String texture) {
        super(0, 0, 0, 0, texture, 1, "back",0);
    }
    public Background(String texture, int frameCount, float frameDuration, AnimatedTexture.PlayMode playMode, float dX, float dY, float width, float height, String name, int layer){
        super(0,0,width,height,texture,1,name,layer);
        deltaX =dX;
        deltaY = dY;
        if(width!=CAM_SIZE)customW=true;
        if(height!=CAM_SIZE)customH=true;
    }
    public void setParallax(float parallaxX,float parallaxY,boolean drawX,boolean drawY){
        this.parallax = new Parallax(parallaxX,parallaxY,CameraManager.GetCamX(),CameraManager.GetCamY());
        this.parallax.drawX = drawX;
        this.parallax.drawY = drawY;
    }
    public void refreshParallax(){
        this.parallax.startX = CameraManager.GetCamX();
        this.parallax.startY = CameraManager.GetCamY();
    }
    @Override
    public void update(float delta){
        texture.update(delta);
        x = CameraManager.GetCamX()- CameraManager.GetViewportWidth()/2 +deltaX
                + (parallax!=null?parallax.getDeltaX(CameraManager.GetCamX()):0);
        y = CameraManager.GetCamY() - CameraManager.GetViewportHeight()/2 + deltaY
                + (parallax!=null?parallax.getDeltaY(CameraManager.GetCamY()):0);
        width = customW?width:CameraManager.GetViewportWidth();
        height = customH?height:CameraManager.GetViewportHeight();
    }
    public void setPaveMode(float tileWidth, float tileHeight) {//ЗАМОСТИТЬ
        mode = DrawMode.PAVE;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
    }

    public void setStretchMode() { //РАСТЯНУТЬ
        mode = DrawMode.STRETCH;
    }
    @Override
    public void draw(SpriteBatch batch) {
        if (mode == DrawMode.STRETCH) {
          /*  if(parallax!=null) {
                //  -width<dx<=0
                //float dx=-(x-((int)(x/width))*width),dy=-(y-((int)(y/height))*height);
                float dx = parallax.getDeltaX(CameraManager.GetCamX()) - width * (parallax.getDeltaX(CameraManager.GetCamX()) / width + 1);
                float dy = parallax.getDeltaY(CameraManager.GetCamY()) - height * (parallax.getDeltaY(CameraManager.GetCamY()) / height + 1);
                float dx1=dx,dy1=dy;
                while (parallax.drawX&&dx1<CameraManager.GetViewportWidth()){
                    batch.draw(texture.getFrame(),CameraManager.GetCamX()-CameraManager.GetViewportWidth()/2+dx1,y,width,height);
                    dx1+=width;
                }
                while(parallax.drawY&&dy1<CameraManager.GetViewportHeight()){
                    if(Math.abs(CameraManager.GetCamY()+dy1-y)>0.01)
                    batch.draw(texture.getFrame(),x,CameraManager.GetCamY()-CameraManager.GetViewportHeight()/2+dy1,width,height);
                    dy1+=height;
                }

            }
*/
            batch.draw(texture.getFrame(), x, y,width,height
                    /*customW?width:CameraManager.GetViewportWidth(),
                     customH?height:CameraManager.GetViewportHeight()*/);
        }
        else {
            int row = (int) (height / tileHeight), col = (int) (width / tileWidth);
            row += (row * tileHeight < height) ? 1 : 0;
            col += (col * tileWidth < width) ? 1 : 0;
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    batch.draw(texture.getFrame(),
                            x + tileWidth * j,
                            y + tileHeight * i,
                            tileWidth,tileHeight);
                }

            }
        }
        /*if(x+width<CameraManager.GetCamX()+CameraManager.GetViewportWidth()/2){x+=width;this.draw(batch);x-=width;}
        else if(x>CameraManager.GetCamX()-CameraManager.GetViewportWidth()/2){x-=width;this.draw(batch);x+=width;}
        if(y+height<CameraManager.GetCamY()+CameraManager.GetViewportHeight()/2){y+=height;this.draw(batch);y-=height;}
        else if(y>CameraManager.GetCamY()-CameraManager.GetViewportHeight()/2){y-=height;this.draw(batch);y+=height;}
        */
    }
    private static class Parallax{
        float parallaxX,parallaxY;
        float startX,startY;
        boolean drawX,drawY;
        public Parallax(float parallaxX,float parallaxY,float startX,float startY){
            this.parallaxX = parallaxX;
            this.parallaxY = parallaxY;
            this.startX = startX;
            this.startY = startY;
        }
        public float getDeltaX(float x){
            return parallaxX*(startX-x);
        }
        public float getDeltaY(float y){
            return parallaxY*(startY-y);
        }
    }
}
