package com.shittygames.actors;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.shittygames.managers.TextureManager;
import com.shittygames.utils.AnimatedTexture;

public class AttachedObject extends MyActor {
    private float angle = 0, dX, dY;
    private float angSpeed;
    MyActor origin;

    public AttachedObject(MyActor origin, float dX, float dY, float width, float height, String texture, String name, int layer) {
        super(0, 0, width, height, texture, 1, name, layer);
        this.dX = dX;
        this.dY = dY;
        this.origin = origin;
    }
    public AttachedObject(MyActor origin, float dX, float dY, float width, float height, String texture, int frameCount, float frameDuration, AnimatedTexture.PlayMode playMode, String name, int layer){
        super(0,0,width,height,texture,frameCount,frameDuration,playMode,1,name,layer);
        this.dX = dX;
        this.dY = dY;
        this.origin = origin;
    }
    public void setOrigin(MyActor origin) {
        this.origin = origin;
    }

    public void setPos(float dX, float dY) {
        this.dX = dX;
        this.dY = dY;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public void setAngularSpeed(float DegreePerSec) {
        this.angSpeed += DegreePerSec;
    }

    public float getAngle() {
        return angle;
    }

    @Override
    public float getX() {
        return origin.getX() + dX;
    }

    @Override
    public float getY() {
        return origin.getY() + dY;
    }

    @Override
    public void update(float delta) {
        texture.update(delta);
        this.setX(origin.getX() + dX);
        this.setY(origin.getY() + dY);
        angle += angSpeed*delta;
    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(texture.getFrame(), flipX ? x - width : x, flipY ? y - height : y,
                width / 2, height / 2,
                width, height, flipX ? -1 : 1, flipY ? -1 : 1, angle);
    }
}
