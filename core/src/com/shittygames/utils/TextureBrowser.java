package com.shittygames.utils;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.shittygames.managers.TextureManager;

public class TextureBrowser extends ApplicationAdapter {
    public static final int BLOCK_SIZE = 150;
    SpriteBatch batch;
    private ShapeRenderer shapeRenderer;

    OrthographicCamera cam;
    Sprite sprite[];
    BitmapFont font;
    StringBuffer buffer;
    int mouseX,mouseY;
    @Override
    public void create() {
        batch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setAutoShapeType(true);
        cam = new OrthographicCamera(1280,720);
        font = new BitmapFont(Gdx.files.internal("UI/white.fnt"));
        buffer = new StringBuffer();
        cam.position.x = cam.viewportWidth/2;
        cam.position.y = cam.viewportHeight/2;
        Gdx.input.setInputProcessor(new MouseWheelInput());
        TextureManager.loadTextures("textures.list");
        sprite = new Sprite[TextureManager.getRegionListSize()];
        double kx=0,ky=0;
        for(int i=0;i<sprite.length;i++){
            sprite[i] = new Sprite(TextureManager.getRegion(i));
            kx = (double)BLOCK_SIZE/TextureManager.getRegion(i).getRegionWidth();
            ky = (double)BLOCK_SIZE/TextureManager.getRegion(i).getRegionHeight();
            if(/*kx<ky*/TextureManager.getRegion(i).getRegionHeight()<TextureManager.getRegion(i).getRegionWidth()){
                sprite[i].setSize(BLOCK_SIZE, (float) (TextureManager.getRegion(i).getRegionHeight()*kx));
            }
            else {
                sprite[i].setSize((float) (TextureManager.getRegion(i).getRegionWidth()*ky),BLOCK_SIZE);
            }
            //System.out.println(sprite[i].getWidth()+" "+sprite[i].getHeight());
            //*/
        }
    }

    static float x,y;
    @Override
    public void render() {
        Gdx.gl.glClearColor(.3f,.3f,.3f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        cam.update();
        /*shapeRenderer.begin();

        for(int i = 0; i< cam.viewportHeight/BLOCK_SIZE; i++){
            x = cam.position.x-cam.viewportWidth/2;
            y = cam.position.y-cam.viewportHeight/2-
                    (cam.position.x-cam.viewportHeight/2)%BLOCK_SIZE +i*BLOCK_SIZE;
            shapeRenderer.line(x,
                    y,x+cam.viewportWidth,y);
        }
        for(int i=0;i<cam.viewportWidth/BLOCK_SIZE;i++){
            x = cam.position.x-cam.viewportWidth/2
                    -(cam.position.x-cam.viewportWidth/2)%BLOCK_SIZE+i*BLOCK_SIZE;
            y = cam.position.y-cam.viewportHeight/2;
            shapeRenderer.line(x,y,x,y+cam.viewportHeight);
        }
        shapeRenderer.x(0,0,BLOCK_SIZE/2);
        shapeRenderer.end();
        */
        batch.setProjectionMatrix(cam.combined);
        batch.begin();
        //batch.disableBlending();
        mouseX = Gdx.input.getX();
        mouseY = (int) (cam.viewportHeight -Gdx.input.getY()+cam.position.y-cam.viewportHeight/2);
        for(int i=0;i<sprite.length;i++){
            sprite[i].draw(batch);
            if(mouseX>=sprite[i].getX()&&
                    mouseX<=sprite[i].getX()+sprite[i].getWidth()&&
                    mouseY>=sprite[i].getY()&&
                    mouseY<=sprite[i].getY()+sprite[i].getHeight())
            {
                buffer.append(TextureManager.getRegion(i).name);
                buffer.append(' ');
                buffer.append(TextureManager.getRegion(i).getRegionWidth());
                buffer.append(';');
                buffer.append(TextureManager.getRegion(i).getRegionHeight());
                font.draw(batch,buffer,sprite[i].getX(),sprite[i].getY());
                buffer.delete(0,buffer.length());
            }
        }
        batch.end();
    }


    @Override
    public void dispose() {

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        int cols = (int) (cam.viewportWidth/BLOCK_SIZE)-1,col_i=0,row=0;
        if(cols==0)cols=1;
        for(int i=0;i<sprite.length;i++){
            sprite[i].setPosition(BLOCK_SIZE*(col_i++),row*BLOCK_SIZE);
            if(col_i==cols){
                row++;
                col_i=0;
            }
        }
        cam.viewportWidth = width;
        cam.viewportHeight= height;
        cam.position.x = cam.viewportWidth/2;
        cam.position.y = cam.viewportHeight/2;
    }
    public class MouseWheelInput implements InputProcessor{

        @Override
        public boolean keyDown(int keycode) {
            return false;
        }

        @Override
        public boolean keyUp(int keycode) {
            return false;
        }

        @Override
        public boolean keyTyped(char character) {
            return false;
        }

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            return false;
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            return false;
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            return false;
        }

        @Override
        public boolean mouseMoved(int screenX, int screenY) {
            return false;
        }

        @Override
        public boolean scrolled(int amount) {
            if(amount>0)cam.position.y -= BLOCK_SIZE;
            else if(amount<0)cam.position.y +=BLOCK_SIZE;
            return false;
        }
    }
}
