package com.shittygames.utils;

public abstract class Trajectory {
    public float t, x0, y0;
    public float x,y;
    private boolean posit;
    public Trajectory(float x0,float y0,boolean positive) {
        this.x0 = x0;
        this.y0 = y0;
        this.posit = positive;
    }
    public final void update(float delta){
        if(posit)t += delta;
        else t-=delta;
        updateX();
        updateY();
    }
    public abstract void updateX();
    public abstract void updateY();


}
