package com.shittygames.utils;

import com.shittygames.actors.MyActor;

public interface EventClass {
    void event(MyActor origin);
}
