package com.shittygames.utils;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.shittygames.managers.TextureManager;

public class AnimatedTexture {
    private TextureRegion texture, drawable;
    private float frameDuration, timer;
    private int frameWidth, frameHeight, count;

    public enum PlayMode {NORMAL, REVERSED, LOOP, LOOP_REVERSED, LOOP_RANDOM, LOOP_PINGPONG}

    ;
    private PlayMode playMode;
    private boolean isEnded = false;

    public AnimatedTexture(String texture, int frameCount, float frameDuration, PlayMode playMode) {
        this.texture = TextureManager.getRegion(TextureManager.getRegionIndex(texture));
        this.count = frameCount;
        this.frameDuration = frameDuration;
        this.playMode = playMode;
        this.frameHeight = this.texture.getRegionHeight();
        this.frameWidth = this.texture.getRegionWidth() / count;
        timer = 0;
        if (count == 1) drawable = null;
        else drawable = new TextureRegion(this.texture.getTexture(), frameWidth, frameHeight);
    }

    public void update(float delta) {
        if (count > 0) {
            timer += delta;

        }
    }

    public TextureRegion getFrame() {
        if (count == 1) return texture;
        switch (playMode) {
            case LOOP: {
                if (timer > count * frameDuration) timer -= count * frameDuration;
                drawable.setRegion(texture.getRegionX() + frameWidth * ((int) (timer / frameDuration)),
                        texture.getRegionY(), frameWidth, frameHeight);
                break;
            }
            case NORMAL: {
                if (timer < count * frameDuration)
                    drawable.setRegion(texture.getRegionX() + frameWidth * ((int) (timer / frameDuration)),
                            texture.getRegionY(), frameWidth, frameHeight);
                else {
                    drawable.setRegion(texture.getRegionX() + frameWidth * (count - 1), texture.getRegionY(), frameWidth, frameHeight);
                    isEnded = true;
                }
                break;
            }
            case REVERSED: {
                if (timer < count * frameDuration)
                    drawable.setRegion(texture.getRegionX() + frameWidth * (count - 1 - (int) (timer / frameDuration)),
                            texture.getRegionY(), frameWidth, frameHeight);
                else {
                    drawable.setRegion(texture.getRegionX(), texture.getRegionY(), frameWidth, frameHeight);
                    isEnded = true;
                }
                break;
            }
            case LOOP_RANDOM: {
                if (timer > frameDuration) {
                    timer = 0;
                    drawable.setRegion(texture.getRegionX() + frameWidth * ((int) (java.lang.Math.random() * count)),
                            texture.getRegionY(), frameWidth, frameHeight);
                }
                break;
            }
            case LOOP_PINGPONG: {
                if (timer > 2 * count * frameDuration) timer -= 2 * count * frameDuration;
                drawable.setRegion(texture.getRegionX() +
                                frameWidth * (timer < count * frameDuration ? (int) (timer / frameDuration) : 2 * count - 1 - (int) (timer / frameDuration)),
                        texture.getRegionY(), frameWidth, frameHeight);
                break;
            }
            case LOOP_REVERSED: {
                if (timer > count * frameDuration) timer -= count * frameDuration;
                drawable.setRegion(texture.getRegionX() + frameWidth * (count - 1 - (int) (timer / frameDuration)),
                        texture.getRegionY(), frameWidth, frameHeight);
                break;
            }

        }
        return drawable;
    }

    public boolean isEnded() {
        return isEnded;
    }
}
