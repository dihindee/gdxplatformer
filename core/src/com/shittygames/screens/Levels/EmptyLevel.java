package com.shittygames.screens.Levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.shittygames.actors.Background;
import com.shittygames.actors.Mobs.Player;
import com.shittygames.actors.RoamingObject;
import com.shittygames.actors.StaticObject;
import com.shittygames.managers.*;
import com.shittygames.utils.AnimatedTexture;
import com.shittygames.utils.Trajectory;

import static com.shittygames.managers.ActorManager.*;
import static com.shittygames.managers.CollisionManager.ABS_WALL;

public class EmptyLevel implements Screen {
    public static final String NONAME = null;
    public static final float G = 40;
    public static final int LANDSCAPE_LAYER = 1, MOB_LAYER = 2, PLAYER_LAYER = 3, FOREGROUND_LAYER = 4;
    private SpriteBatch batch;
    private ShapeRenderer shapeRenderer;


    private BitmapFont font;
    StringBuffer crds;

    private static final int GRID_SIZE = 100;

    @Override
    public void show() {
        TextureManager.loadTextures("textures.list");
        batch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();
        crds = new StringBuffer();
        shapeRenderer.setColor(0, 0, 0, 1);
        shapeRenderer.setAutoShapeType(true);
        CameraManager.InitCamera();
        ActorManager.Initialize();
        AudioManager.Initialize();
        ScriptManager.LoadResources();
        font = new BitmapFont();


        // Add actors here




        batch.setProjectionMatrix(CameraManager.GetCamera().combined);
        font = new BitmapFont();
        font.setColor(Color.BLACK);
    }

    static float x, y; //||\\

    @Override
    public void render(float delta) {
        //camera.update();
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) CameraManager.SetCamScale(1.05f);
        else if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) CameraManager.SetCamScale(0.95f);
        CameraManager.CamUpdate();
        batch.setProjectionMatrix(CameraManager.GetCamera().combined);
        shapeRenderer.setProjectionMatrix(CameraManager.GetCamera().combined);
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        ScriptManager.UserInput();
        ScriptManager.ScriptUpdate();
        ActorManager.Step(batch, delta);
        x = (CameraManager.GetCamX() - CameraManager.GetViewportWidth() / 2 + CameraManager.GetViewportWidth() * Gdx.input.getX() / Gdx.graphics.getWidth());
        y = (CameraManager.GetCamY() + CameraManager.GetViewportHeight() / 2 - CameraManager.GetViewportHeight() * Gdx.input.getY() / Gdx.graphics.getHeight());
        crds.append((int) x);
        crds.append(';');
        crds.append((int) y);
        font.draw(batch, crds, x + 5, y);
        crds.delete(0, crds.length());
        batch.end();
        shapeRenderer.begin();

        for (int i = 0; i < CameraManager.GetViewportHeight() / GRID_SIZE; i++) {
            x = CameraManager.GetCamX() - CameraManager.GetViewportWidth() / 2;
            y = CameraManager.GetCamY() - CameraManager.GetViewportHeight() / 2 -
                    (CameraManager.GetCamY() - CameraManager.GetViewportHeight() / 2) % GRID_SIZE + i * GRID_SIZE;
            shapeRenderer.line(x,
                    y, x + CameraManager.GetViewportWidth(), y);
        }
        for (int i = 0; i < CameraManager.GetViewportWidth() / GRID_SIZE; i++) {
            x = CameraManager.GetCamX() - CameraManager.GetViewportWidth() / 2
                    - (CameraManager.GetCamX() - CameraManager.GetViewportWidth() / 2) % GRID_SIZE + i * GRID_SIZE;
            y = CameraManager.GetCamY() - CameraManager.GetViewportHeight() / 2;
            shapeRenderer.line(x, y, x, y + CameraManager.GetViewportHeight());
        }
        shapeRenderer.x(0, 0, GRID_SIZE / 2);
        shapeRenderer.end();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        TextureManager.disposeTextures();
        AudioManager.dispose();
    }
}
