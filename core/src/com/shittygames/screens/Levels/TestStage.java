package com.shittygames.screens.Levels;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.shittygames.actors.Background;
import com.shittygames.actors.Mobs.Player;
import com.shittygames.actors.Mobs.TestBot;
import com.shittygames.actors.RoamingObject;
import com.shittygames.actors.StaticObject;
import com.shittygames.managers.*;
import com.shittygames.utils.AnimatedTexture;
import com.shittygames.utils.Trajectory;

import static com.shittygames.managers.ActorManager.*;
import static com.shittygames.managers.CollisionManager.ABS_WALL;


public class TestStage implements Screen{
    public static final String NONAME=null;
	public static final float G = 40;
	public static final int LANDSCAPE_LAYER=1,MOB_LAYER=2,PLAYER_LAYER=3,FOREGROUND_LAYER=4;
	private SpriteBatch batch;
	private ShapeRenderer shapeRenderer;


	private BitmapFont font;
	StringBuffer crds;

	private static final int GRID_SIZE=100;

	@Override
	public void show() {
		TextureManager.loadTextures("textures.list");
		batch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		crds = new StringBuffer();
		shapeRenderer.setColor(0,0,0,1);
		shapeRenderer.setAutoShapeType(true);
		CameraManager.InitCamera();
		ActorManager.Initialize();
		AudioManager.Initialize();
		ScriptManager.LoadResources();
		font = new BitmapFont();
		//Add(new Background(6));
		AddPlayer(new Player(200,200,4));
		Add(new StaticObject(0,0,4800,128,"stone",0,null,1));
		((StaticObject)getLast()).setPaveMode(64,64);
		Add(new Background("back"));
		((Background)getLast()).setPaveMode(64,64);
		//((Background)getLast()).setParallax(.1f,0,true,true);
		Add(new RoamingObject(64,64,"explosion-1",8,0.1f,AnimatedTexture.PlayMode.LOOP_PINGPONG,1,"penis",2));
		((RoamingObject)getLast()).setTrajectory(new Trajectory(360,360,true) {
			@Override
			public void updateX() {
				x = -(float)(200*Math.sin(t))+x0;
			}

			@Override
			public void updateY() {
				y = (float)(200*Math.cos(t))+y0;
			}
		});
		//Add(new RoamingObject(128,128,"stone",0,null,5));
		Add(new StaticObject(600,128,128,128,"explosion-4",12,0.1f, AnimatedTexture.PlayMode.NORMAL,1,null,10));
		Add(new StaticObject(1000,550,100,100,"stone",ABS_WALL,null,2));

		Add(new StaticObject(900,400,512,64,"upper-platform",0,null,5));

		CameraManager.CamFollow(getPlayer());
		//CameraManager.CamFollow();
		//CameraManager.LookAt(600,600);
		//CameraManager.SetCamScale();
		//CameraManager.SetCamScale(1.5f);
		batch.setProjectionMatrix(CameraManager.GetCamera().combined);
		font = new BitmapFont();
		font.setColor(Color.BLACK);
	}
	static	float x,y; //||\\
	@Override
	public void render(float delta) {
		//camera.update();
		if(Gdx.input.isKeyPressed(Input.Keys.UP))CameraManager.SetCamScale(1.05f);
			else if(Gdx.input.isKeyPressed(Input.Keys.DOWN))CameraManager.SetCamScale(0.95f);
		CameraManager.CamUpdate();
		batch.setProjectionMatrix(CameraManager.GetCamera().combined);
		shapeRenderer.setProjectionMatrix(CameraManager.GetCamera().combined);
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		ScriptManager.UserInput();
		ScriptManager.ScriptUpdate();
		ActorManager.Step(batch,delta);
		x = (CameraManager.GetCamX()-CameraManager.GetViewportWidth()/2+CameraManager.GetViewportWidth()*Gdx.input.getX()/Gdx.graphics.getWidth());
		y = (CameraManager.GetCamY()+CameraManager.GetViewportHeight()/2-CameraManager.GetViewportHeight()*Gdx.input.getY()/Gdx.graphics.getHeight());
		crds.append((int)x);
		crds.append(';');
		crds.append((int)y);
		font.draw(batch,crds,x+10*CameraManager.zoom,y);
		crds.delete(0,crds.length());
		batch.end();
		shapeRenderer.begin();

		for(int i=0;i<CameraManager.GetViewportHeight()/GRID_SIZE;i++){
			x = CameraManager.GetCamX()-CameraManager.GetViewportWidth()/2;
			y = CameraManager.GetCamY()-CameraManager.GetViewportHeight()/2-
					(CameraManager.GetCamY()-CameraManager.GetViewportHeight()/2)%GRID_SIZE +i*GRID_SIZE;
			shapeRenderer.line(x,
					y,x+CameraManager.GetViewportWidth(),y);
		}
		for(int i=0;i<CameraManager.GetViewportWidth()/GRID_SIZE;i++){
			x = CameraManager.GetCamX()-CameraManager.GetViewportWidth()/2
			-(CameraManager.GetCamX()-CameraManager.GetViewportWidth()/2)%GRID_SIZE+i*GRID_SIZE;
			y = CameraManager.GetCamY()-CameraManager.GetViewportHeight()/2;
			shapeRenderer.line(x,y,x,y+CameraManager.GetViewportHeight());
		}
		shapeRenderer.x(0,0,GRID_SIZE/2);
		shapeRenderer.end();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		TextureManager.disposeTextures();
		AudioManager.dispose();
	}
}



